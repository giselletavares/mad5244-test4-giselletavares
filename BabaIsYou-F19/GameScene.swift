//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var baba: SKSpriteNode!
    var flag: SKSpriteNode!
    var flagblock: SKSpriteNode!
    var winblock: SKSpriteNode!
    var isblock: SKSpriteNode!
    var isblocktwo: SKSpriteNode!
    var wallblock: SKSpriteNode!
    var stopblock: SKSpriteNode!
    var wallone: SKSpriteNode!
    var walltwo: SKSpriteNode!
    var wallthree: SKSpriteNode!
    var wallfour: SKSpriteNode!
    var PLAYER_SPEED: CGFloat! = 32
    var border: CGFloat! = 1
    var congratulations: SKLabelNode!
    

    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        
        self.baba = self.childNode(withName: "baba") as? SKSpriteNode
        self.flag = self.childNode(withName: "flag") as? SKSpriteNode
        self.flagblock = self.childNode(withName: "flagblock") as? SKSpriteNode
        self.winblock = self.childNode(withName: "winblock") as? SKSpriteNode
        self.isblock = self.childNode(withName: "isblock") as? SKSpriteNode
        self.isblocktwo = self.childNode(withName: "isblocktwo") as? SKSpriteNode
        self.wallblock = self.childNode(withName: "wallblock") as? SKSpriteNode
        self.stopblock = self.childNode(withName: "stopblock") as? SKSpriteNode
        self.wallone = self.childNode(withName: "wallone") as? SKSpriteNode
        self.walltwo = self.childNode(withName: "walltwo") as? SKSpriteNode
        self.wallthree = self.childNode(withName: "wallthree") as? SKSpriteNode
        self.wallfour = self.childNode(withName: "wallfour") as? SKSpriteNode
        self.congratulations = self.childNode(withName: "congratulations") as? SKLabelNode
    }
   
    func didBegin(_ contact: SKPhysicsContact) {
        print("Something collided!")
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        if((abs(round(wallblock.position.y) - round(isblock.position.y)) <= 10 && abs(round(wallblock.position.y) - round(stopblock.position.y)) <= 10)
            && abs(round(isblock.position.x) - round(wallblock.position.x + 64)) <= 1 && abs(round(stopblock.position.x) - round(isblock.position.x + 64)) <= 1){
            
            print("wall is stop")
            baba.physicsBody?.collisionBitMask = 506 // 2 + 8 + 16 + 32 + 64 + 128 + 256
            wallone.physicsBody?.collisionBitMask = 1
            walltwo.physicsBody?.collisionBitMask = 1
            wallthree.physicsBody?.collisionBitMask = 1
            wallfour.physicsBody?.collisionBitMask = 1
            
        } else if((abs(round(flagblock.position.y) - round(isblock.position.y)) <= 10 && abs(round(flagblock.position.y) - round(stopblock.position.y)) <= 10)
            && abs(round(isblock.position.x) - round(flagblock.position.x + 64)) <= 1 && abs(round(stopblock.position.x) - round(isblock.position.x + 64)) <= 1){
            
            print("flag is stop")
            baba.physicsBody?.collisionBitMask = 508 // 4 + 8 + 16 + 32 + 64 + 128 + 256
            flag.physicsBody?.collisionBitMask = 1
            
        } else {
            baba.physicsBody?.collisionBitMask = 504
            wallone.physicsBody?.collisionBitMask = 0
            walltwo.physicsBody?.collisionBitMask = 0
            wallthree.physicsBody?.collisionBitMask = 0
            wallfour.physicsBody?.collisionBitMask = 0
            flag.physicsBody?.collisionBitMask = 0
        }
        
        // flag is win
        if(abs(round(flagblock.position.y) - round(isblocktwo.position.y)) <= 10 && abs(round(flagblock.position.y) - round(winblock.position.y)) <= 10
            && abs(round(isblocktwo.position.x) - round(flagblock.position.x + 64)) <= 1 && abs(round(winblock.position.x) - round(isblocktwo.position.x + 64)) <= 1){
            
            print("flag is win")

            if(baba.frame.intersects(flag.frame)){
                congratulations.text = "CONGRATULATIONS"
            }
        }
        
        // wall is win
        if(abs(round(wallblock.position.y) - round(isblocktwo.position.y)) <= 10 && abs(round(wallblock.position.y) - round(winblock.position.y)) <= 10
            && abs(round(isblocktwo.position.x) - round(wallblock.position.x + 64)) <= 1 && abs(round(winblock.position.x) - round(isblocktwo.position.x + 64)) <= 1){
            
            print("wall is win")

            if(baba.frame.intersects(wallone.frame) || baba.frame.intersects(walltwo.frame)
                || baba.frame.intersects(wallthree.frame) || baba.frame.intersects(wallfour.frame)){
                congratulations.text = "CONGRATULATIONS"
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let mouseTouch = touches.first else { return }
                
        let location = mouseTouch.location(in: self)
        let nodeTouched = atPoint(location).name
        
        if(nodeTouched == "up_button") {
            baba.position.y += PLAYER_SPEED
        } else if (nodeTouched == "down_button"){
            baba.position.y -= PLAYER_SPEED
        } else if (nodeTouched == "left_button"){
            baba.position.x -= PLAYER_SPEED
        } else if (nodeTouched == "right_button"){
            baba.position.x += PLAYER_SPEED
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}
